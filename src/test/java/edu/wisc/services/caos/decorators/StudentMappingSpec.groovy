package edu.wisc.services.caos.decorators

import edu.wisc.services.caos.AcademicObjective
import edu.wisc.services.caos.AcademicObjectiveTerm
import edu.wisc.services.caos.Career
import edu.wisc.services.caos.CodeDescriptionPairType
import edu.wisc.services.caos.PrimaryCareer
import edu.wisc.services.caos.SchoolCollege
import edu.wisc.services.caos.Student
import edu.wisc.services.caos.StudentPlan
import edu.wisc.services.caos.StudentProgram
import edu.wisc.services.caos.Term
import groovy.transform.CompileDynamic
import spock.lang.Specification

@CompileDynamic
class StudentMappingSpec extends Specification {

  def 'should correctly update degree specifics for current undergrad students'() {
    when: 'A current undergrad student'
    final AcademicRecord academicRecord = new AcademicRecord(
      allTerms: [ new Term(termCode: '1') ],
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  code: 'programWithDegree',
                  isPrimaryProgramInTerm: true,
                  schoolCollege: new SchoolCollege(formalDescription: 'Letters and Science'),
                  studentPlans: [
                    new StudentPlan(
                      awardCategory: ProgramData.BACHELORS_PLAN,
                      code: 'ART 081',
                      transcriptDescription: 'Art',
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.MAJ.name())
                    ),
                    new StudentPlan(
                      awardCategory: ProgramData.UNDERGRADUATE_CERTIFICATE_PLAN,
                      code: 'LCRT353',
                      transcriptDescription: 'Certificate in Entrepreneurship',
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.CRT.name())
                    )
                  ]
                ),
                new StudentProgram(
                  studentPlans: [
                    new StudentPlan(
                      awardCategory: ProgramData.BACHELORS_PLAN,
                      code: 'BA  445',
                      transcriptDescription: 'French',
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.MAJ.name())
                    )
                  ]
                )
              ],
              term: new CodeDescriptionPairType(code: '1')
            ),
          ]
        ),
        primaryCareer: new PrimaryCareer(termCode: 1, enrollmentStatusCode: AcademicRecord.ENROLLED_ENROLLMENT_STATUS),
      )
    )
    then:
    academicRecord.degreeLevel == 'Bachelor\'s'
    academicRecord.studentProgram.schoolCollege?.formalDescription == 'Letters and Science'
    academicRecord.majors.containsAll('Art', 'French')
    academicRecord.minors.containsAll('Certificate in Entrepreneurship')
  }

  def 'should correctly update degree specifics for current non-undergrad students'() {
    when: 'A non-undergrad student'
    final AcademicRecord academicRecord = new AcademicRecord(
      allTerms: [ new Term(termCode: '1') ],
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              term: new CodeDescriptionPairType(code: '1'),
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'MEDS'),
                  code: 'programWithDegree',
                  isPrimaryProgramInTerm: true,
                  schoolCollege: new SchoolCollege(formalDescription: 'UW-Madison'),
                  studentPlans: [
                    new StudentPlan(
                      awardCategory: ProgramData.CLINICAL_DOCTORATE_PLAN,
                      code: 'MED 666',
                      transcriptDescription: 'Medicine',
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.MAJ.name())
                    )
                  ]
                ),
                new StudentProgram(
                  studentPlans: [new StudentPlan(code: 'MCRT397')]
                )
              ]
            ),
          ]
        ),
        primaryCareer: new PrimaryCareer(termCode: 1, enrollmentStatusCode: AcademicRecord.ENROLLED_ENROLLMENT_STATUS),
      )
    )
    then:
    academicRecord.degreeLevel == 'Clinical Doctorate'
    academicRecord.studentProgram.schoolCollege?.formalDescription == 'UW-Madison'
    academicRecord.majors.containsAll('Medicine')
    academicRecord.minors.empty //Certificate code in extraProgram should not have been processed leaving minors empty
  }

}
