package edu.wisc.services.caos.decorators

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl
import edu.wisc.services.caos.AcademicObjective
import edu.wisc.services.caos.AcademicObjectiveTerm
import edu.wisc.services.caos.Career
import edu.wisc.services.caos.CodeDescriptionPairType
import edu.wisc.services.caos.DegreeCheckoutStatusType
import edu.wisc.services.caos.EntryEvent
import edu.wisc.services.caos.EntryInformation
import edu.wisc.services.caos.PrimaryCareer
import edu.wisc.services.caos.Student
import edu.wisc.services.caos.StudentProgram
import edu.wisc.services.caos.Term
import groovy.transform.CompileDynamic
import groovy.util.logging.Slf4j
import spock.lang.Specification

import javax.xml.datatype.XMLGregorianCalendar

@CompileDynamic
class StartDateMappingSpec extends Specification {

  def "should correctly set start date for current student"() {
    given:
    XMLGregorianCalendar startCal = new XMLGregorianCalendarImpl(year: 2014, month: 12, day: 23)
    when:
    AcademicRecord academicRecord = new AcademicRecord(
      allTerms: [
        new Term( beginDate: startCal, termCode: '1154' ),
      ],
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  expectedGraduationTerm: new StudentProgram.ExpectedGraduationTerm(code: '1184'),
                  isPrimaryProgramInTerm: true
                )
              ],
              term: new CodeDescriptionPairType(code: '1184')
            )
          ]
        ),
        entryInformation: new EntryInformation(
          careers: [
            new Career(code: 'UGRD', entryEvents: [ new EntryEvent(termCode: '1154', type: 'matriculation') ]),
            new Career(code: 'USPC')
          ]
        ),
        primaryCareer: new PrimaryCareer(termCode: '1184', enrollmentStatusCode: AcademicRecord.ENROLLED_ENROLLMENT_STATUS)
      )
    )
    then:
    academicRecord.matriculationDate == startCal.toGregorianCalendar().time
  }

  def "should correctly set start date for alumni"() {
    given:
    XMLGregorianCalendar startCal = new XMLGregorianCalendarImpl(year: 2014, month: 12, day: 23)
    when:
    AcademicRecord academicRecord = new AcademicRecord(
      allTerms: [
        new Term(beginDate: startCal, termCode: '1154'),
      ],
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  degreeCheckoutStatus: new DegreeCheckoutStatusType(
                    code: 'AW',
                  ),
                  isPrimaryProgramInTerm: true
                )
              ],
              term: new CodeDescriptionPairType(code: '1184')
            )
          ]
        ),
        entryInformation: new EntryInformation(
          careers: [
            new Career(code: 'UGRD', entryEvents: [ new EntryEvent(termCode: '1154', type: 'matriculation') ]),
            new Career(code: 'USPC')
          ]
        )
      )
    )
    then:
    academicRecord.matriculationDate == startCal.toGregorianCalendar().time
    }

  def "should correctly set estimated graduation date for current students"() {
    given:
    XMLGregorianCalendar endCal = new XMLGregorianCalendarImpl(year: 2014, month: 12, day: 23)
    when:
    AcademicRecord academicRecord = new AcademicRecord(
      allTerms: [
        new Term(endDate: endCal, termCode: '1184'),
      ],
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  expectedGraduationTerm: new StudentProgram.ExpectedGraduationTerm(code: '1184'),
                  isPrimaryProgramInTerm: true
                )
              ],
              term: new CodeDescriptionPairType(code: '1184')
            )
          ]
        ),
        entryInformation: new EntryInformation(
          careers: [
            new Career(code: 'UGRD', entryEvents: [new EntryEvent(termCode: '1154', type: 'matriculation')]),
            new Career(code: 'USPC')
          ]
        ),
        primaryCareer: new PrimaryCareer(termCode: '1184', enrollmentStatusCode: AcademicRecord.ENROLLED_ENROLLMENT_STATUS)
      )
    )
    then:
    academicRecord.estimatedGraduationDate == endCal.toGregorianCalendar().time
  }

  def "should correctly set estimated graduation date for alumni"() {
    given:
    XMLGregorianCalendar endCal = new XMLGregorianCalendarImpl(year: 2014, month: 12, day: 23)
    when:
    AcademicRecord academicRecord = new AcademicRecord(
      allTerms: [
        new Term(endDate: endCal, termCode: '1184'),
      ],
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  degreeCheckoutStatus: new DegreeCheckoutStatusType(
                    degreeConferredDate: endCal,
                    code: 'AW',
                  ),
                  isPrimaryProgramInTerm: true
                )
              ],
              term: new CodeDescriptionPairType(code: '1184')
            )
          ]
        ),
        entryInformation: new EntryInformation(
          careers: [
            new Career(code: 'UGRD', entryEvents: [new EntryEvent(termCode: '1154', type: 'matriculation')]),
            new Career(code: 'USPC')
          ]
        )
      )
    )
    then:
    academicRecord.estimatedGraduationDate == endCal.toGregorianCalendar().time

  }
}
