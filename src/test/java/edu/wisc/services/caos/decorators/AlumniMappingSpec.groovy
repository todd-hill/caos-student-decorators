package edu.wisc.services.caos.decorators

import edu.wisc.services.caos.*
import groovy.transform.CompileDynamic
import spock.lang.Specification

@CompileDynamic
class AlumniMappingSpec extends Specification {

  def 'An ALUMNUS gets the information from their most recently awarded degree'(){
    when: 'an academic record with multiple awards'
    final AcademicRecord academicRecord = new AcademicRecord(
      caosStudent: new Student(
        academicObjective: new AcademicObjective(
          academicObjectiveTerms: [
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  code: 'programWithDegree',
                  degreeCheckoutStatus: new DegreeCheckoutStatusType(code: 'AW'),
                  isPrimaryProgramInTerm: true,
                  studentPlans: [
                    new StudentPlan(
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.MAJ),
                      transcriptDescription: 'Quantum Shenanigans'
                    )
                  ]
                )
              ],
              term: new CodeDescriptionPairType(code: '1')
            ),
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  code: 'programWithDegree',
                  degreeCheckoutStatus: new DegreeCheckoutStatusType(code: 'AW'),
                  isPrimaryProgramInTerm: true,
                  studentPlans: [
                    new StudentPlan(
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.MAJ),
                      transcriptDescription: 'Underwater Basketweaving'
                    )
                  ]
                )
              ],
              term: new CodeDescriptionPairType(code: '2') // most recent with an award
            ),
            new AcademicObjectiveTerm(
              studentPrograms: [
                new StudentProgram(
                  career: new Career(code: 'UGRD'),
                  code: 'programWithDegree',
                  isPrimaryProgramInTerm: true,
                  studentPlans: [
                    new StudentPlan(
                      type: new CodeDescriptionPairType(code: PlanTypeEnum.MAJ),
                      transcriptDescription: 'Forensic Urology'
                    )
                  ]
                )
              ],
              term: new CodeDescriptionPairType(code: '3') // most recent term, but not awarded
            )
          ]
        )
      ),
      allTerms: [
        new Term(termCode: '1'),
        new Term(termCode: '2'),
        new Term(termCode: '3')
      ]
    )
    then: 'the most recent awarded program is represented'
    academicRecord.majors.size() == 1
    academicRecord.majors.first() == 'Underwater Basketweaving'
  }

}
