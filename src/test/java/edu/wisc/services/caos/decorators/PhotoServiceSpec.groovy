package edu.wisc.services.caos.decorators

import edu.wisc.services.caos.decorators.photos.PhotoService
import okhttp3.OkHttpClient
import org.junit.Ignore
import spock.lang.Specification

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

/**
 * Expects actual connection info
 */
@Ignore
class PhotoServiceSpec extends Specification {

  private OkHttpClient httpClient
  private PhotoService photoService

  def setup() {
    this.httpClient = CaosConfig.httpClient(10, 120, 60, 'NONE')
    this.photoService = new PhotoService('url', 'user', 'password', httpClient)
  }

  def 'write image to file'() {
    given:
      String url = 'https://photo.services.wisc.edu/delivery/i/p/example-cat.png'

    when:
      BufferedImage image = photoService.getPhotoSync(url)
      Boolean foundWriter = ImageIO.write(image, "png", new File('target/example-cat.png'))
      File photo = new File('target/example-cat.png')

    then:
      foundWriter
      photo.size() > 0
  }

}
