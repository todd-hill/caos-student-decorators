package edu.wisc.services.caos.decorators

import groovy.transform.CompileStatic

@CompileStatic
enum PlanTypeEnum {

  MAJ, MIN, CAP, SP, CRT, TCH

}
