package edu.wisc.services.caos.decorators

import edu.wisc.services.caos.AcademicObjectiveTerm
import edu.wisc.services.caos.StudentPlan
import edu.wisc.services.caos.StudentProgram
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@CompileStatic
@Slf4j
class ProgramData implements Comparable<ProgramData> {

  public static final String CAPSTONE_PROGRAM = 'UNCS'
  public static final String UNDERGRADUATE_CERTIFICATE_PLAN = 'Undergraduate Certificate'
  public static final String GRADUATE_PROFESSIONAL_CERTIFICATE_PLAN = 'Graduate/Professional Certificate'
  public static final String CAPSTONE_PLAN = 'Capstone'
  public static final String BACHELORS_PLAN = 'Bachelor\'s'
  public static final String MASTERS_PLAN = 'Master\'s'
  public static final String RESEARCH_DOCTORATE_PLAN = 'Research Doctorate'
  public static final String CLINICAL_DOCTORATE_PLAN = 'Clinical Doctorate'

  private static final List<String> PLANS = [
    UNDERGRADUATE_CERTIFICATE_PLAN,
    GRADUATE_PROFESSIONAL_CERTIFICATE_PLAN,
    CAPSTONE_PLAN,
    BACHELORS_PLAN,
    MASTERS_PLAN,
    RESEARCH_DOCTORATE_PLAN,
    CLINICAL_DOCTORATE_PLAN
  ]

  @Delegate StudentProgram studentProgram

  @Delegate AcademicObjectiveTerm academicObjectiveTerm

  Set<String> getMajors() {
    this?.studentPlans?.findResults { getMajor(it) } as Set
  }

  Set<String> getMinors() {
    this?.studentPlans?.findResults { getMinor(it) } as Set
  }

  String getAwardCategory() {
    this.studentPlans?.max { PLANS.indexOf(it?.awardCategory) }?.awardCategory
  }

  @Override
  int compareTo(ProgramData other) {
    if (other == null) {
      return 1
    }
    return this.term.code <=> other.term.code ?:
      PLANS.indexOf(this.awardCategory) <=> PLANS.indexOf(other.awardCategory) ?:
        this.schoolCollege?.shortDescription <=> other.schoolCollege?.shortDescription ?:
          this.code <=> other.code
  }

  private static String getMajor(StudentPlan plan) {
    return plan?.type?.code == PlanTypeEnum.MAJ.name() ? plan?.transcriptDescription : null
  }

  private static String getMinor(StudentPlan plan) {
    plan?.type?.code == PlanTypeEnum.CRT.name() ? plan?.transcriptDescription : null
  }

}
