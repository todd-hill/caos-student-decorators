package edu.wisc.services.caos.decorators

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory
import org.springframework.ws.transport.WebServiceMessageSender
import org.springframework.ws.transport.http.ClientHttpRequestMessageSender

import java.util.concurrent.TimeUnit

@CompileStatic
@Configuration
@Import(edu.wisc.services.caos.client.CaosConfig)
@Slf4j
class CaosConfig {

  @Bean
  @Primary
  static WebServiceMessageSender messageSender(OkHttpClient httpClient) {
    return new ClientHttpRequestMessageSender(new OkHttp3ClientHttpRequestFactory(httpClient))
  }

  @Bean
  static OkHttpClient httpClient(
    @Value('${caos.connectTimeoutSeconds:10}') Integer connectTimeout,
    @Value('${caos.readTimeoutSeconds:120}') Integer readTimeout,
    @Value('${caos.writeTimeoutSeconds:60}') Integer writeTimeout,
    @Value('${caos.wireLoggingLevel:NONE}') String wireLoggingLevel
  ) {
    final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor()
    if (wireLoggingLevel != null) {
      try {
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.valueOf(wireLoggingLevel.toUpperCase()))
      } catch (IllegalArgumentException e){
        log.error("[$wireLoggingLevel] is not a valid string value for OkHttp3 logging, should be one of NONE, BASIC, HEADERS, OR BODY", e)
        log.info("Setting logging value to NONE")
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
      }
    }
    new OkHttpClient.Builder()
      .addInterceptor(loggingInterceptor)
      .cookieJar(new JavaNetCookieJar(new CookieManager()))
      .connectTimeout(connectTimeout, TimeUnit.SECONDS)
      .followRedirects(false)
      .readTimeout(readTimeout, TimeUnit.SECONDS)
      .writeTimeout(writeTimeout, TimeUnit.SECONDS)
      .build()
  }

}
