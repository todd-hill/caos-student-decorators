package edu.wisc.services.caos.decorators

import edu.wisc.services.caos.AcademicObjectiveTerm
import edu.wisc.services.caos.Advisor
import edu.wisc.services.caos.Student
import edu.wisc.services.caos.Term
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.lang.NonNull

@CompileStatic
@Slf4j
class AcademicRecord {

  public static final String AWARDED_CHECKOUT_STATUS = "AW"
  public static final String CAPSTONE_PROGRAM = "UNCS"
  public static final String ELIGIBLE_ENROLLMENT_STATUS = 'ELIG'
  public static final String ENROLLED_ENROLLMENT_STATUS = 'ENRL'
  public static final String FISC_PROGRAM = 'UNFS'
  public static final String MATRICULATION_EVENT = 'matriculation'
  public static final String UNDERGRAD_CAREER = 'UGRD'

  @Delegate
  @NonNull
  Student caosStudent

  List<Term> allTerms

  BigDecimal getGpa() {
    if (!this.primaryCareerCode || !this.academicSummary) {
      return null
    }
    return this
      ?.academicSummary
      ?.find { it.career.code == this.primaryCareerCode }
      ?.careerCumulatives
      ?.cumulativeGPA ?: null
  }

  Boolean hasFiscDegree() {
    awardedPrograms?.any { it.code == FISC_PROGRAM }
  }

  Boolean isCurrent() {
    primaryCareer?.enrollmentStatusCode in [ENROLLED_ENROLLMENT_STATUS, ELIGIBLE_ENROLLMENT_STATUS]
  }

  Boolean isAlumni() {
    this.awardedPrograms
  }

  Date getEstimatedGraduationDate() {
    this.getEstimatedGraduationTerm()?.endDate?.toGregorianCalendar()?.time
  }

  Term getEstimatedGraduationTerm() {
    if (this.isCurrent()) {
      this.getTermByTermCode(this.studentProgram?.expectedGraduationTerm?.code)
    } else {
      this.studentProgram?.degreeCheckoutStatus?.degreeConferredDate ? this.getTermByTermCode(this.academicObjectiveTerm?.term?.code) : null
    }
  }

  Date getMatriculationDate() {
    this.getMatriculationTerm()?.beginDate?.toGregorianCalendar()?.time
  }

  Term getMatriculationTerm() {
    this.getTermByTermCode(this.entryTermCode)
  }

  String getAcademicLevel() {
    this.primaryCareer?.academicLevelDescription
  }

  String getDegreeLevel() {
    this.studentProgram?.awardCategory
  }

  List<ProgramData> getStudentPrograms() {
    this
      ?.academicObjectiveTerm
      ?.studentPrograms
      ?.collect { new ProgramData(academicObjectiveTerm: this.academicObjectiveTerm, studentProgram: it) }
  }

  ProgramData getStudentProgram() {
    this
      .academicObjectiveTerm
      ?.studentPrograms
      ?.findAll { it?.isPrimaryProgramInTerm }
      ?.collect { new ProgramData( academicObjectiveTerm: this.academicObjectiveTerm, studentProgram: it) }
      ?.max()
  }

  List<ProgramData> getAwardedCapstones() {
    this.awardedPrograms.findAll { it.studentProgram.code == CAPSTONE_PROGRAM }
  }

  List<ProgramData> getAwardedPrograms() {
    this.academicTerms.collectMany { term ->
      term
        ?.studentPrograms
        ?.findAll { it?.degreeCheckoutStatus?.code == AWARDED_CHECKOUT_STATUS }
        ?.collect { new ProgramData(studentProgram: it, academicObjectiveTerm: term) }
    }
  }

  Set<String> getMajors() {
    // TODO: get majors and minors within the same degree—we can only guess knowing that undergrads are less likely to pursue multiple degrees
    final List<ProgramData> programs = this.undergrad? this.studentPrograms : [this.studentProgram]
    programs?.findResults { it?.majors }?.flatten() as Set
  }

  Set<String> getMinors() {
    // TODO: get majors and minors within the same degree—we can only guess knowing that undergrads are less likely to pursue multiple degrees
    final List<ProgramData> programs = this.undergrad? this.studentPrograms : [this.studentProgram]
    programs?.findResults { it?.minors }?.flatten() as Set
  }

  Set<Advisor> getAdvisors() {
    this
      ?.studentAdvisorRelationships
      ?.collect { it.advisor } as Set
  }

  Set<Advisor> getAcademicAdvisors() {
    this
      ?.studentAdvisorRelationships
      ?.findAll { it.role.code == 'ADVR' }
      ?.collect { it.advisor } as Set
  }

  private AcademicObjectiveTerm getAcademicObjectiveTerm() {
    if (this.isCurrent() && this.primaryTermCode && this.academicTerms) {
      return this
        .academicTerms
        .toSorted { it.term.code }
        .with { it.find { it.term.code == this.primaryTermCode } ?: it.find { it.term.code > this.primaryTermCode } }
    } else if (this.isAlumni()) {
      return this
        .academicTerms
        .find { it?.term?.code == this.mostRecentPrimaryAwardedProgram?.term?.code }
    }
    return null
  }

  private Boolean isUndergrad() {
    this.primaryCareerCode == UNDERGRAD_CAREER
  }

  private List<AcademicObjectiveTerm> getAcademicTerms() {
    if (!this.academicObjective) {
      return []
    }
    this.academicObjective.academicObjectiveTerms
  }

  private List<ProgramData> getPrimaryAwardedPrograms() {
    this.awardedPrograms?.findAll { it.isPrimaryProgramInTerm }
  }

  private ProgramData getMostRecentPrimaryAwardedProgram() {
    this.primaryAwardedPrograms?.max()
  }

  private String getEntryTermCode() {
    this
      .entryInformation
      ?.careers
      ?.find { it?.code == this.primaryCareerCode }
      ?.entryEvents
      ?.find { it?.type == MATRICULATION_EVENT }
      ?.termCode
  }

  private String getPrimaryCareerCode() {
    this.studentProgram?.career?.code
  }

  private String getPrimaryTermCode() {
    primaryCareer?.termCode
  }

  private Term getTermByTermCode(String termCode) {
    if (!termCode) {
      return null
    }
    this.allTerms.find { it.termCode == termCode }
  }

}
