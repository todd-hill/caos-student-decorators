package edu.wisc.services.caos.decorators

import groovy.transform.CompileStatic

/**
 * Enumeration of career codes.
 * Note that the ordinal value indicates the precedence with 0 (MEDS) being the highest
 */
@CompileStatic
enum CareerCodeEnum {

  MEDS, VMED, LAW, GRAD, PHAR, UGRD, USPC, UGST, NSTD, PREC

  static List<CareerCodeEnum> professionAndGrad(){
    [MEDS, VMED, LAW, GRAD, PHAR]
  }

}
