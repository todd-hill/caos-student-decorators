package edu.wisc.services.caos.decorators

import groovy.transform.CompileStatic
import groovy.transform.Immutable

@CompileStatic
@Immutable
class VisaPermitTypes {

  static final String F1 = 'F-1'
  static final String J1 = 'J-1'

}
