package edu.wisc.services.caos.decorators.photos

import com.google.common.util.concurrent.RateLimiter
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import okhttp3.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers

import javax.imageio.ImageIO
import javax.security.auth.login.FailedLoginException
import java.awt.image.BufferedImage

@CompileStatic
@Component
@Slf4j
class PhotoService {

  private static final String RELAY_STATE_PATTERN = /(?<=name="RelayState" value="cookie&#x3a;)[^"]*(?=")/
  private static final String SAML_RESPONSE_PATTERN = /(?<=name="SAMLResponse" value=")[^"]*(?=")/

  private final OkHttpClient httpClient
  private final String url
  private final String username
  private final String password

  volatile private RateLimiter rateLimiter
  volatile private Boolean authenticated

  PhotoService(
    @Value('${photo.service.url:https://photo.services.wisc.edu}') String url,
    @Value('${photo.service.username:}') String username,
    @Value('${photo.service.password:}') String password,
    @Value('${photo.service.requestsPerSecond:5}') Double rateLimit,
    OkHttpClient httpClient
  ) {
    this.httpClient = httpClient
    this.url = url
    this.username = username
    this.password = password
    this.rateLimiter = RateLimiter.create(rateLimit)
  }

  Mono<BufferedImage> retrievePhoto(String photoUrl, Integer remainingAttempts = 3) {
    Mono
      .fromCallable { this.retrievePhotoSync(photoUrl, remainingAttempts) }
      .subscribeOn(Schedulers.newElastic('photo'))
  }

  BufferedImage retrievePhotoSync(String photoUrl, Integer remainingAttempts = 3) {
    if (remainingAttempts < 1) {
      log.warn("Gave up after retries for url ${photoUrl}")
      return null
    }
    login()
    InputStream responseStream = null
    try {
      rateLimiter.acquire()
      Response response = httpClient.newCall(new Request.Builder().url(photoUrl).build()).execute()
      responseStream = response.body().byteStream()
      switch(response.code()) {
        case 302:
        case 401:
          this.authenticated = false
          return retrievePhotoSync(photoUrl, --remainingAttempts)
        case 200:
          this.authenticated = true
          if (response.header('Content-Type') != 'image/png') {
            return null
          }
          return ImageIO.read(responseStream)
        default:
          return null
      }
    }
    catch (IOException e) {
      log.error("Failed to get image ${photoUrl}", e)
      return retrievePhotoSync(photoUrl, remainingAttempts)
    }
    finally {
      responseStream?.close()
    }
  }

  private synchronized void login(Integer attempts = 3) throws FailedLoginException {
    if (attempts < 1) {
      throw new FailedLoginException('Failed to login to photo service after retries')
    }
    Response response1 = null
    Response response2 = null
    Response response3 = null
    Response response4 = null
    Response response5 = null
    try {
      if (authenticated) {
        return
      }
      // Get SAMLRequest from photo.services.wisc.edu
      Request request1 = new Request.Builder().url("$url/docs").build()
      rateLimiter.acquire()
      response1 = httpClient.newCall(request1).execute()
      String location = response1.header('Location')
      if (response1.code() != 302 || !location) {
        return
      }
      log.debug('Authenticating with NetID login')
      // Send SAMLRequest to login.wisc.edu
      Request request2 = new Request.Builder().url(location).build()
      rateLimiter.acquire()
      response2 = httpClient.newCall(request2).execute()
      // Get Login form (do we have to?)
      Request request3 = new Request.Builder().url(response2.header('Location')).build()
      rateLimiter.acquire()
      response3 = httpClient.newCall(request3).execute()
      // Submit login form
      Request request4 = new Request.Builder()
        .url(response2.header('Location'))
        .post(RequestBody.create(
          MediaType.parse( 'application/x-www-form-urlencoded'),
          "j_username=$username&j_password=$password&_eventId_proceed="
        ))
        .build()
      rateLimiter.acquire()
      response4 = httpClient.newCall(request4).execute()
      // GET SAMLResponse from login.wisc.edu
      String responseBody4 = response4.body().string()
      String relayState = URLEncoder.encode('cookie:' + (responseBody4 =~ RELAY_STATE_PATTERN)[0], 'UTF-8')
      String samlResponse = URLEncoder.encode((responseBody4 =~ SAML_RESPONSE_PATTERN)[0].toString(), 'UTF-8')
      // Send SAMLResponse to photo.services.wisc.edu
      Request request5 = new Request.Builder()
        .url("$url/Shibboleth.sso/SAML2/POST")
        .post(RequestBody.create(
          MediaType.parse('application/x-www-form-urlencoded'),
          "RelayState=$relayState&SAMLResponse=$samlResponse"
        ))
        .build()
      rateLimiter.acquire()
      response5 = httpClient.newCall(request5).execute()
      this.authenticated = true
    }
    catch(Exception e) {
      log.error('Failed to login to photo-service', e)
      login(--attempts)
    }
    finally {
      response1?.close()
      response2?.close()
      response3?.close()
      response4?.close()
      response5?.close()
    }
  }

}
