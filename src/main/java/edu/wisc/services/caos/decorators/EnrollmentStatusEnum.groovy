package edu.wisc.services.caos.decorators

import groovy.transform.CompileStatic

@CompileStatic
enum EnrollmentStatusEnum {

  ADMT, APPL, CONN, ELIG, ENRL, INTD, WDRW

}
